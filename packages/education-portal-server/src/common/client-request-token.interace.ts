export interface ClientHttpRequestTokenInterface {

  token: {
    client_id?: string;
    trusted_client?: number;
    active?: boolean;
    exp?: number;
    name?: string;
    email?: string;
    sub?: string;
    scope?: Array<string>;
    roles?: Array<string>;
    accessToken?: string;
    clientId?: string;
    trustedClient?: number;
    _id?: any;
  }

} 