import { AddStudentCommandHandler } from "./add-student/add-student.handler";
import { RemoveStudentCommandHandler } from "./remove-student/remove-student.handler";
import { UpdateStudentCommandHandler } from "./update-student/update-student.handler";

export const StudentCommandManager = [AddStudentCommandHandler, RemoveStudentCommandHandler, UpdateStudentCommandHandler];
