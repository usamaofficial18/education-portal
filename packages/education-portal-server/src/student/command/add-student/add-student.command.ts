import { ICommand } from '@nestjs/cqrs';
import { StudentDto } from '../../entity/student/student-dto';

export class AddStudentCommand implements ICommand {
  constructor(
    public studentPayload: StudentDto,
    public readonly clientHttpRequest: any,
  ) {}
}