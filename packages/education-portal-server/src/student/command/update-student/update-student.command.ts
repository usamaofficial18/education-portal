import { ICommand } from '@nestjs/cqrs';
import { UpdateStudentDto } from '../../entity/student/update-student-dto';

export class UpdateStudentCommand implements ICommand {
  constructor(public readonly updatePayload:UpdateStudentDto ) {}
}
