import { Test, TestingModule } from '@nestjs/testing';
import { StudentAggregateService } from './student-aggregate.service';
describe('generaCqrslAggregateService', () => {
  let service: StudentAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StudentAggregateService],
    }).compile();

    service = module.get<StudentAggregateService>(StudentAggregateService);
  });
  StudentAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
