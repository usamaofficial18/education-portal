import { StudentAddedEventHandler } from "./student-added/student-added.handler";
import { StudentRemovedEventHandler } from "./student-removed/student.removed.handler";
import { StudentUpdatedEventHandler } from "./student-updated/student-updated.handler";

export const StudentEventManager = [StudentAddedEventHandler,StudentRemovedEventHandler,StudentUpdatedEventHandler];
